## catalyst-cxx-11

C++ consumers of the Catalyst library are expected to use C++11 to handle the
move semantics provided.
